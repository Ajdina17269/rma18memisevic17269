package com.example.spirala1.knjige;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.IOException;

public class DodavanjeKnjigeFragment extends Fragment {

    Button dodajSliku;
    Button dodajKnjigu;
    Button nazad;
    EditText nazivKnjige;
    EditText imeAutora;
    Spinner spinner;
    ImageView slika;

    ArrayAdapter adapterSpinner;

    String picturePath;

    KnjigeFragmentInterface knjigeFragmentInterface;

    public DodavanjeKnjigeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dodavanje_knjige, container, false);

        knjigeFragmentInterface = (KnjigeFragmentInterface)getActivity();

        dodajSliku = view.findViewById(R.id.dNadjiSliku);
        dodajKnjigu = view.findViewById(R.id.dUpisiKnjigu);
        nazad = view.findViewById(R.id.dPonisti);
        nazivKnjige = view.findViewById(R.id.nazivKnjige);
        imeAutora = view.findViewById(R.id.imeAutora);
        spinner = view.findViewById(R.id.sKategorijaKnjige);
        slika = view.findViewById(R.id.naslovnaStr);

        adapterSpinner = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, Container.getInstance().kategorije);
        spinner.setAdapter(adapterSpinner);

        Container.getInstance().pop = false;

        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!nazivKnjige.getText().toString().isEmpty() && !imeAutora.getText().toString().isEmpty() && !Container.getInstance().kategorije.isEmpty()) {
                    Knjiga k = new Knjiga(nazivKnjige.getText().toString(), imeAutora.getText().toString(), Container.getInstance().kategorije.get(spinner.getSelectedItemPosition()), picturePath);
                    KategorijeAkt.baza.dodajKnjigu(k);
                    KategorijeAkt.refreshujListe();
                    knjigeFragmentInterface.onPovratak();
                }
            }
        });

        dodajSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                } else {
                    intent = new Intent(Intent.ACTION_GET_CONTENT);
                }
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Odaberi sliku"), 1);
            }
        });

        nazad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                knjigeFragmentInterface.onPovratak();
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Uri uri = data.getData();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                final int takeFlags = data.getFlags() & Intent.FLAG_GRANT_READ_URI_PERMISSION;
                ContentResolver resolver = getActivity().getContentResolver();
                resolver.takePersistableUriPermission(uri, takeFlags);
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    slika.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                picturePath = uri.toString();
            }
        } catch (Exception e) {
            Log.d("error", e.toString());
        }
    }
}
