package com.example.spirala1.knjige;



public interface ListeFragmentInterface {
    void dodajKnjigu();
    void izlistajKnjige(String tekst,boolean autori);
}
