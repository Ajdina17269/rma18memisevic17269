package com.example.spirala1.knjige;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import java.util.ArrayList;

public class KnjigeReciever<T> extends ResultReceiver {

    public interface KnjigeReceiverCallBack<T> {
        void finish(T data);
        void error();
        void start();
    }

    public KnjigeReciever(Handler handler) {
        super(handler);
    }

    private KnjigeReceiverCallBack callBack;

    public void setCallBack(KnjigeReceiverCallBack<T> callBack) {
        this.callBack = callBack;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        if (callBack != null) {
            switch (resultCode) {
                case KnjigePoznanika.STATUS_START:
                    callBack.start();
                    break;
                case KnjigePoznanika.STATUS_ERROR:
                    callBack.error();
                    break;
                case KnjigePoznanika.STATUS_FINISH:
                    callBack.finish(resultData.getParcelableArrayList("knjige"));
                    break;
            }
        }
    }
}
