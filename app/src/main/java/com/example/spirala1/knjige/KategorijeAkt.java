package com.example.spirala1.knjige;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.HashSet;

public class KategorijeAkt extends FragmentActivity implements ListeFragmentInterface, KnjigeFragmentInterface,AdapterKnjiga.KlikNaPreporuci {

    FragmentManager fragmentManager;

    FrameLayout mainFrame;
    FrameLayout secFrame;
    Button dodajOnline;

    int orientation;
    public static KnjigeBazaPodataka baza;


    public static void refreshujListe(){
        Container.getInstance().kategorije=baza.DajSveKategorije();
        Container.getInstance().knjige=baza.dajSveKnjige();
        Container.getInstance().autori=baza.dajSveAutore();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorije_akt);



        baza=new KnjigeBazaPodataka(this);

        refreshujListe();


        mainFrame = findViewById(R.id.mainFrame);
        secFrame = findViewById(R.id.secFrame);
        dodajOnline = findViewById(R.id.dDodajOnline);

        fragmentManager = getSupportFragmentManager();



        dodajOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentOnline fo = new FragmentOnline();
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("listaKategorija",Container.getInstance().kategorije);

                fo.setArguments(bundle);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.mainFrame, fo)
                        .addToBackStack(null)
                        .commit();
            }
        });

        orientation = getResources().getConfiguration().orientation;

        if(orientation == Configuration.ORIENTATION_LANDSCAPE){
            KnjigeFragment knjigeFragment = (KnjigeFragment) fragmentManager.findFragmentByTag("knjigeFragment");
            if(knjigeFragment == null){
                Bundle bundle = new Bundle();
                bundle.putInt("Pozicija", -1);
                knjigeFragment = new KnjigeFragment();
                knjigeFragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.secFrame, knjigeFragment, "knjigeFragment").commit();
            }
            if(Container.getInstance().pop)
                fragmentManager.popBackStack();
            else {
                secFrame.setVisibility(View.GONE);
            }
        }

        ListeFragment listeFragment = (ListeFragment) fragmentManager.findFragmentByTag("listaFragment");
        if(listeFragment == null){
            listeFragment = new ListeFragment();
            fragmentManager.beginTransaction().replace(R.id.mainFrame, listeFragment, "listaFragment").commit();
        }
    }

    @Override
    public void dodajKnjigu() {
        if(orientation == Configuration.ORIENTATION_LANDSCAPE){
            secFrame.setVisibility(View.GONE);
        }
        DodavanjeKnjigeFragment dodavanjeKnjigeFragment = new DodavanjeKnjigeFragment();
        fragmentManager.beginTransaction().replace(R.id.mainFrame, dodavanjeKnjigeFragment, "dodavanjeKnjigeFragment").addToBackStack(null).commit();
    }

    @Override
    public void izlistajKnjige(String tekst,boolean autori) {
        ArrayList<Knjiga> knjige;
        if(autori){
            long id = baza.DajIDAutora(tekst);
            knjige=baza.knjigeAutora(id);
        }
        else{
            long id=baza.dajIDKategorije(tekst);
            knjige=baza.knjigeKategorije(id);
        }

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("Knjige", knjige);
        KnjigeFragment knjigeFragment = new KnjigeFragment();
        knjigeFragment.setArguments(bundle);
        if(orientation == Configuration.ORIENTATION_LANDSCAPE){
            fragmentManager.beginTransaction().replace(R.id.secFrame, knjigeFragment, "knjigeFragment").commit();
        } else {
            fragmentManager.beginTransaction().replace(R.id.mainFrame, knjigeFragment, "knjigeFragment").addToBackStack(null).commit();
        }
    }

    @Override
    public void onPovratak() {
        if(orientation == Configuration.ORIENTATION_LANDSCAPE && secFrame.getVisibility() == View.GONE){
            secFrame.setVisibility(View.VISIBLE);
        }
        fragmentManager.popBackStack();
        Container.getInstance().pop = true;
    }
/*
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(orientation == Configuration.ORIENTATION_LANDSCAPE && secFrame.getVisibility() == View.GONE){
            secFrame.setVisibility(View.VISIBLE);
        }
        fragmentManager.popBackStack();
        Container.getInstance().pop = true;
    }
*/
    @Override
    public void PreporuciKlik(String id) {
        Bundle arguments = new Bundle();
        arguments.putString("idKnjige",id);
        arguments.putStringArrayList("emailovi",getNameEmailDetails());
        arguments.putStringArrayList("imena",getNames());

         PreporuciFragment preprouceniFragment = new PreporuciFragment();
         preprouceniFragment.setArguments(arguments);


        fragmentManager.beginTransaction().replace(R.id.mainFrame, preprouceniFragment,"preporuciFragment").addToBackStack(null).commit();


    }


    public ArrayList<String> getNameEmailDetails() {
        ArrayList<String> emlRecs = new ArrayList<String>();
        HashSet<String> emlRecsHS = new HashSet<String>();
        Context context = this;
        ContentResolver cr = context.getContentResolver();
        String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Photo.CONTACT_ID };
        String order = "CASE WHEN "
                + ContactsContract.Contacts.DISPLAY_NAME
                + " NOT LIKE '%@%' THEN 1 ELSE 2 END, "
                + ContactsContract.Contacts.DISPLAY_NAME
                + ", "
                + ContactsContract.CommonDataKinds.Email.DATA
                + " COLLATE NOCASE";
        String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
        Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter, null, order);
        if (cur.moveToFirst()) {
            do {
                // names comes in hand sometimes
                String name = cur.getString(1);
                String emlAddr = cur.getString(3);

                // keep unique only
                if (emlRecsHS.add(emlAddr.toLowerCase())) {
                    emlRecs.add(emlAddr);
                }
            } while (cur.moveToNext());
        }

        cur.close();
        return emlRecs;
    }


    public ArrayList<String> getNames() {
        ArrayList<String> emlRecs = new ArrayList<String>();
        HashSet<String> emlRecsHS = new HashSet<String>();
        Context context = this;
        ContentResolver cr = context.getContentResolver();
        String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Photo.CONTACT_ID };
        String order = "CASE WHEN "
                + ContactsContract.Contacts.DISPLAY_NAME
                + " NOT LIKE '%@%' THEN 1 ELSE 2 END, "
                + ContactsContract.Contacts.DISPLAY_NAME
                + ", "
                + ContactsContract.CommonDataKinds.Email.DATA
                + " COLLATE NOCASE";
        String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
        Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter, null, order);
        if (cur.moveToFirst()) {
            do {
                // names comes in hand sometimes
                String name = cur.getString(1);
                String emlAddr = cur.getString(3);

                // keep unique only
                if (emlRecsHS.add(emlAddr.toLowerCase())) {
                    emlRecs.add(name);
                }
            } while (cur.moveToNext());
        }

        cur.close();
        return emlRecs;
    }
}
