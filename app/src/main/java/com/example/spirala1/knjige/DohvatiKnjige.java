package com.example.spirala1.knjige;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class DohvatiKnjige extends AsyncTask<String, Integer, Void> {
    private ArrayList<Knjiga> listaKnjiga = new ArrayList<>();

    private IDohvatiKnjigeDone dohvatiKnjigeDone;

    public DohvatiKnjige(IDohvatiKnjigeDone dohvatiKnjigeDone) {
        this.dohvatiKnjigeDone = dohvatiKnjigeDone;
    }

    @Override
    protected Void doInBackground(String... strings) {
        String query;
        try {
            for (String naziv : strings) {
                query = URLEncoder.encode(naziv, "utf-8");
                String urlString = "https://www.googleapis.com/books/v1/volumes?maxResults=5&q=intitle:" + query;
                URL url = new URL(urlString);
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                String rezultat = convertInputToString(inputStream);
                JSONArray results = new JSONObject(rezultat).getJSONArray("items");

                for (int i = 0; i < results.length(); i++) {
                    JSONObject knjiga = results.getJSONObject(i);
                    JSONObject volumeInfo = knjiga.getJSONObject("volumeInfo");
                    ArrayList<Autor> autori = new ArrayList<>();
                    try {
                        JSONArray authors = volumeInfo.getJSONArray("authors");
                        for (int j = 0; j < authors.length(); j++) {
                            autori.add(new Autor(authors.getString(j), knjiga.getString("id")));
                        }
                    } catch (JSONException ignored) {

                    }


                    String description = "";
                    try {
                        description = volumeInfo.getString("description");
                    } catch (JSONException e) {
                    }

                    String publishedDate = "";
                    try {
                        publishedDate = volumeInfo.getString("publishedDate");
                    } catch (JSONException e) {
                    }

                    // URL
                    URL url2 = null;
                    try {
                        url2 = new URL(volumeInfo.getJSONObject("imageLinks").getString("thumbnail"));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    int pageCount = 0;
                    try {
                        pageCount = volumeInfo.getInt("pageCount");
                    } catch (JSONException e) {
                    }

                    listaKnjiga.add(new Knjiga(
                            knjiga.getString("id"),
                            volumeInfo.getString("title"),
                            autori,
                            description,
                            publishedDate,
                            url2,
                            pageCount));
                }
            }
            dohvatiKnjigeDone.onDohvatiDone(listaKnjiga);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String convertInputToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
