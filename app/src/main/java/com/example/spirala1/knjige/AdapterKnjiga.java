package com.example.spirala1.knjige;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.List;

public class AdapterKnjiga extends ArrayAdapter {

    int _resource;
    Context _context;
    private KlikNaPreporuci klikic;

    public interface KlikNaPreporuci{
        void PreporuciKlik(String id);
    }

    public AdapterKnjiga(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        _context = context;
        _resource = resource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout newView;
        final Knjiga item = (Knjiga) getItem(position);
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater) getContext().
                    getSystemService(inflater);
            li.inflate(_resource, newView, true);
        } else {
            newView = (LinearLayout) convertView;
        }

        try{
            klikic = (KlikNaPreporuci) newView.getContext();

        }catch(ClassCastException e){
            throw new ClassCastException(convertView.getContext().toString() + " treba implementirati OnButtonClick");
        }

        ImageView slika = newView.findViewById(R.id.eNaslovna);
        TextView knjiga = newView.findViewById(R.id.eNaziv);
        TextView autor = newView.findViewById(R.id.eAutor);
        TextView opis = newView.findViewById(R.id.eOpis);
        TextView datumObjave = newView.findViewById(R.id.eDatumObjavljivanja);
        TextView brojStranica = newView.findViewById(R.id.eBrojStranica);
        Button preporuciB = newView.findViewById(R.id.dPreporuci);

        opis.setText(item.getOpis());
        datumObjave.setText(item.getDatumObjavljivanja());
        brojStranica.setText(String.valueOf(item.getBrojStrinica()));
        preporuciB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                klikic.PreporuciKlik(item.getId());
            }
        });
        knjiga.setText(item.getNaziv());
        autor.setText(item.getAutori().get(0).getImeiPrezime());
        if(item.getObojena())
            newView.setBackgroundColor(_context.getResources().getColor(R.color.coloredBook));
        if(item.getSlikaPath() != null) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(_context.getContentResolver(), Uri.parse(item.getSlikaPath()));
                slika.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            Glide.with(getContext()).load(item.getSlika().toString()).into(slika);
        }




        return newView;
    }
}
