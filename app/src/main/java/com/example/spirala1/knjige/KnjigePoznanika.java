package com.example.spirala1.knjige;


import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class KnjigePoznanika extends IntentService {

    public static final int STATUS_START=0;
    public static final int STATUS_FINISH=1;
    public static final int STATUS_ERROR=2;

    private ArrayList<Knjiga> vraceneKnjige;


    public KnjigePoznanika(){
        super(null);
    }


    public KnjigePoznanika(String name) {
        super(name);
    }

    @Override
    public void onCreate(){
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {


        final ResultReceiver reciever = intent.getParcelableExtra("receiver");
        final String userId = intent.getStringExtra("idKorisnika");
        vraceneKnjige=new ArrayList<>();
        Bundle bundle = new Bundle();
        reciever.send(STATUS_START,Bundle.EMPTY);
        try {
            URL url = new URL("https://www.googleapis.com/books/v1/users/" + userId + "/bookshelves");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("items");

            for(int i=0; i<items.length(); i++){
                String policaURL = items.getJSONObject(i).getString("selfLink");
                URL url1 = new URL(policaURL+"/volumes");
                HttpURLConnection urlConnection1 = (HttpURLConnection) url1.openConnection();
                InputStream in1 = new BufferedInputStream(urlConnection1.getInputStream());
                String rezultat1 = convertStreamToString(in1);
                JSONObject jo1 = new JSONObject(rezultat1);
                JSONArray items1 = jo1.getJSONArray("items");

                String id = "", naziv = "", datumObjave = "", opis = "";
                int brojStranica = 0;
                ArrayList<Autor> autori = null;
                URL slika = null;

                for (int k = 0; k < items1.length(); k++) {
                    JSONObject knjiga = items1.getJSONObject(k);

                    //ID
                    if (knjiga.has("id"))
                        id = knjiga.getString("id");
                    JSONObject izdanje = knjiga.getJSONObject("volumeInfo");

                    //NASLOV
                    if (izdanje.has("title"))
                        naziv = izdanje.getString("title");

                    //AUTORI
                    if (izdanje.has("authors")) {
                        autori = new ArrayList<Autor>();
                        JSONArray autoriArray = izdanje.getJSONArray("authors");
                        for (int j = 0; j < autoriArray.length(); j++) {
                            Autor autor = new Autor(autoriArray.getString(j), 1);
                            autor.dodajKnjigu(id);
                            autori.add(autor);
                        }
                    }


                    //DATUM OBJAVE
                    if (izdanje.has("publishedDate"))
                        datumObjave = izdanje.getString("publishedDate");

                    //OPIS
                    if (izdanje.has("description"))
                        opis = izdanje.getString("description");

                    //BROJ STRANICA
                    if (izdanje.has("pageCount"))
                        brojStranica = izdanje.getInt("pageCount");


                    //SLIKA

                    if(izdanje.has("imageLinks")){
                        JSONObject objekat = izdanje.getJSONObject("imageLinks");
                        if(objekat.has("thumbnail")) {
                            String slikaURL = objekat.getString("thumbnail");
                            slika = new URL(slikaURL);
                        }

                    }
                    if(slika==null)
                        slika = new URL("https://cdn-images-1.medium.com/max/2000/1*Y2HOjUAyd6MYR7dDFARbBQ.jpeg");


                    Knjiga rezz = new Knjiga(id, naziv, autori, opis, datumObjave, slika, brojStranica);



                    vraceneKnjige.add(rezz);

                }


            }

        }
        catch(Exception e){
            e.printStackTrace();
            bundle.putString("greska",e.getMessage());
            reciever.send(STATUS_ERROR,bundle);
            return;
        }
        bundle.putParcelableArrayList("knjige",vraceneKnjige);
        reciever.send(STATUS_FINISH,bundle);


    }


    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line+"\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

}