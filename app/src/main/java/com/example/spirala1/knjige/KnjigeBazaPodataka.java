package com.example.spirala1.knjige;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;


import java.net.URL;
import java.util.ArrayList;

public class KnjigeBazaPodataka extends SQLiteOpenHelper {


    // Verzija baze
    private static final int DATABASE_VERSION = 1;

    // Ime baze
    private static final String DATABASE_NAME = "contactsManager";

    // Imena tabela
    private static final String TABELA_KATEGORIJA = "Kategorija";
    private static final String TABELA_KNJIGA= "Knjiga";
    private static final String TABELA_AUTOR = "Autor";
    private static final String TABELA_AUTORSTVO="Autorstvo";

    // Zajednicka imena kolona nekih tabela
    private static final String KEY_ID = "_id";
    private static final String NAZIV="naziv";

    // Kategorija Knjiga - nazivi kolona
    private static final String OPIS = "opis";
    private static final String DATUM = "datumObjavljivanja";
    private static final String STRANICE = "brojStranica";
    private static final String ID_WEBSERVIS  = "idWebServis";
    private static final String ID_KATEGORIJE = "idKategorije";
    private static final String SLIKA = "slika";
    private static final String PREGLEDANA = "pregledana";


    // AUTOR Tabela - imena kolona
    private static final String IME = "ime";

    // AUTORSTVO Tabela - imena knjige
    private static final String ID_AUTORA = "idautora";
    private static final String ID_KNJIGE = "idknjige";


    KnjigeBazaPodataka(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }



    // Todo table create statement
    private static final String CREATE_TABLE_KATEGORIJA = "CREATE TABLE "
            + TABELA_KATEGORIJA + "(" + KEY_ID + " INTEGER PRIMARY KEY," + NAZIV
            + " TEXT)";


    private static final String CREATE_TABLE_KNJIGA = "CREATE TABLE "
            + TABELA_KNJIGA + "(" + KEY_ID + " INTEGER PRIMARY KEY," + NAZIV
            + " TEXT,"+ OPIS + " TEXT," + DATUM + " TEXT," + STRANICE
            + " INTEGER," + ID_WEBSERVIS + " TEXT," + ID_KATEGORIJE + " INTEGER," + SLIKA + " TEXT,"+ PREGLEDANA + " INTEGER)";

    private static final String CREATE_TABLE_AUTOR = "CREATE TABLE "
            + TABELA_AUTOR + "(" + KEY_ID + " INTEGER PRIMARY KEY," + IME
            + " TEXT)";

    private static final String CREATE_TABLE_AUTORSTVO = "CREATE TABLE "
            + TABELA_AUTORSTVO + "(" + KEY_ID + " INTEGER," + ID_AUTORA + " INTEGER," + ID_KNJIGE
            + " INTEGER)";


    long dodajKategoriju(String naziv){

        SQLiteDatabase baza = this.getReadableDatabase();
        String query = "SELECT * FROM Kategorija WHERE naziv = ?";
        Cursor c = baza.rawQuery(query,new String[]{naziv});


        if(c.getCount()>0) return -1; //vec postoji kategorija
        c.close();


        ContentValues cv = new ContentValues();
        cv.put(NAZIV,naziv);
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            return db.insert(TABELA_KATEGORIJA,null,cv);
        }
        catch(Exception e){
            return -1;
        }
    }


    long dajIDKategorije(String kategorija){
        long rez=0;
        String query3 = "SELECT * FROM Kategorija WHERE naziv=?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query3,new String[]{kategorija});
        if(c.moveToFirst()){
            do{
                rez = c.getInt(c.getColumnIndex(KEY_ID));
            }while(c.moveToNext());
        }
        c.close();
        return rez;
    }


    public Autor dajAutora(int id){
        String query3 = "SELECT * FROM Autor WHERE _id=?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query3,new String[]{String.valueOf(id)});
        Autor rez = new Autor("", 1);
        if(c.moveToFirst()){
            do{
                rez.setImeiPrezime(c.getString(c.getColumnIndex(IME)));
            }while(c.moveToNext());
        }
        c.close();
        return rez;
    }


    public long DajIDAutora(String imePrezime){
        long rez=0;
        String query3 = "SELECT * FROM Autor WHERE ime=?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query3,new String[]{imePrezime});
        if(c.moveToFirst()){
            do{
                rez = c.getInt(c.getColumnIndex(KEY_ID));
            }while(c.moveToNext());
        }
        c.close();
        return rez;
    }





    ArrayList<Knjiga> knjigeKategorije(long idKategorije){
        ArrayList<Knjiga> rezultat = new ArrayList<>();

        String query = "SELECT * FROM Knjiga WHERE idKategorije=?";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(query,new String[]{String.valueOf(idKategorije)});

        String nazivKateg;

        if(c.moveToFirst()){
            query = "SELECT naziv FROM Kategorija WHERE _id=?";

            Cursor c2 = db.rawQuery(query,new String[]{String.valueOf(idKategorije)});
            c2.moveToFirst();
            nazivKateg=c2.getString(c2.getColumnIndex(NAZIV));
            c2.close();
            do{
                //pokupimo pisce prvo:



                ArrayList<Autor> autori = new ArrayList<>();
                //prvo sve njihove id-ove
                String query2 = "SELECT * FROM Autorstvo WHERE idknjige=?";
                Cursor c3 = db.rawQuery(query2,new String[]{String.valueOf(c.getInt(c.getColumnIndex(KEY_ID)))});

                if(c3.moveToFirst()){
                    do{
                        autori.add(dajAutora(c3.getInt(c3.getColumnIndex(ID_AUTORA))));

                    }while(c3.moveToNext());
                    c3.close();
                }

                String nazivKnjige = c.getString(c.getColumnIndex(NAZIV));
                String datumObjave = c.getString(c.getColumnIndex(DATUM));
                int brojStrana = c.getInt(c.getColumnIndex(STRANICE));
                String urlPath = c.getString(c.getColumnIndex(SLIKA));
                URL uri = null;
                try {
                    uri = new URL(urlPath);
                }
                catch(Exception e){

                }

                String id = c.getString(c.getColumnIndex(ID_WEBSERVIS));
                int pregledana = c.getInt(c.getColumnIndex(PREGLEDANA));
                String opis = c.getString(c.getColumnIndex(OPIS));

                Knjiga k = new Knjiga(id,nazivKnjige,autori,opis,datumObjave,uri,brojStrana);
                if(pregledana==1)
                    k.setObojena(true);

                k.setKategorija(nazivKateg);

                rezultat.add(k);

            }while(c.moveToNext());
        }
        c.close();

        return rezultat;
    }


    ArrayList<Autor> dajSveAutore() {
        ArrayList<Autor> autori = new ArrayList<>();
        String query = "SELECT * FROM Autor";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query,new String[]{});

        if(c.moveToFirst()){
            do{
                Autor a = new Autor(c.getString(c.getColumnIndex(IME)),1);

                String query2 = "SELECT * FROM Autorstvo WHERE idautora=?";
                Cursor c2=db.rawQuery(query2,new String[]{String.valueOf(c.getInt(c.getColumnIndex(KEY_ID)))});
                c2.moveToFirst();
                a.setBrojKnjiga(c2.getCount());
                c2.close();
                autori.add(new Autor(c.getString(c.getColumnIndex(IME)),1));
            }while(c.moveToNext());
        }
        c.close();;
        return autori;
    }


    ArrayList<Knjiga> dajSveKnjige(){
        ArrayList<Knjiga> rezultat = new ArrayList<>();
        String query = "SELECT * FROM Knjiga";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query,new String[]{});

        if(c.moveToFirst()){
            do {

                ArrayList<Autor> autori = new ArrayList<>();
                //prvo sve njihove id-ove
                String query1 = "SELECT * FROM Autorstvo WHERE idknjige=?";
                Cursor c6 = db.rawQuery(query1,new String[]{String.valueOf(c.getInt(c.getColumnIndex(KEY_ID)))});
                if(c6.moveToFirst()){
                    do{

                        autori.add(dajAutora(c6.getInt(c6.getColumnIndex(ID_AUTORA))));

                    }while(c6.moveToNext());
                    c6.close();
                }

                String nazivKnjige = c.getString(c.getColumnIndex(NAZIV));
                String datumObjave = c.getString(c.getColumnIndex(DATUM));
                int brojStrana = c.getInt(c.getColumnIndex(STRANICE));
                String urlPath = c.getString(c.getColumnIndex(SLIKA));
                URL uri = null;
                try {
                    uri = new URL(urlPath);
                }
                catch(Exception e){

                }

                String id = c.getString(c.getColumnIndex(ID_WEBSERVIS));
                int pregledana = c.getInt(c.getColumnIndex(PREGLEDANA));
                String opis = c.getString(c.getColumnIndex(OPIS));

                Knjiga k = new Knjiga(id,nazivKnjige,autori,opis,datumObjave,uri,brojStrana);
                if(pregledana==1)
                    k.setObojena(true);

                Cursor c2 = db.rawQuery("SELECT * FROM Kategorija WHERE _id=?",new String[]{String.valueOf(c.getInt(c.getColumnIndex(ID_KATEGORIJE)))});
                c2.moveToFirst();
                String nazivKateg=c2.getString(c2.getColumnIndex(NAZIV));
                c2.close();

                k.setKategorija(nazivKateg);

                rezultat.add(k);

            }while(c.moveToNext());
        }
        c.close();
        return rezultat;
    }




    ArrayList<Knjiga> knjigeAutora(long idAutora){
        ArrayList<Knjiga> rezultat = new ArrayList<>();

        String query2 = "SELECT * FROM Autorstvo WHERE idautora=?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c3 = db.rawQuery(query2,new String[]{String.valueOf(idAutora)});

        if(c3.moveToFirst()){
            do{
                String query = "SELECT * FROM Knjiga WHERE _id=?";
                Cursor c = db.rawQuery(query, new String[]{String.valueOf(c3.getInt(c3.getColumnIndex(ID_KNJIGE)))});

                if(c.moveToFirst()){

                    ArrayList<Autor> autori = new ArrayList<>();
                    //prvo sve njihove id-ove
                    String query1 = "SELECT * FROM Autorstvo WHERE idknjige=?";
                    Cursor c6 = db.rawQuery(query1,new String[]{String.valueOf(c.getInt(c.getColumnIndex(KEY_ID)))});
                    if(c6.moveToFirst()){
                        do{
                            autori.add(dajAutora(c6.getInt(c6.getColumnIndex(ID_AUTORA))));

                        }while(c6.moveToNext());
                        c6.close();
                    }

                    String nazivKnjige = c.getString(c.getColumnIndex(NAZIV));
                    String datumObjave = c.getString(c.getColumnIndex(DATUM));
                    int brojStrana = c.getInt(c.getColumnIndex(STRANICE));
                    String urlPath = c.getString(c.getColumnIndex(SLIKA));
                    URL uri = null;
                    try {
                        uri = new URL(urlPath);
                    }
                    catch(Exception e){

                    }

                    String id = c.getString(c.getColumnIndex(ID_WEBSERVIS));
                    int pregledana = c.getInt(c.getColumnIndex(PREGLEDANA));
                    String opis = c.getString(c.getColumnIndex(OPIS));

                    Knjiga k = new Knjiga(id,nazivKnjige,autori,opis,datumObjave,uri,brojStrana);
                    if(pregledana==1)
                        k.setObojena(true);

                    Cursor c2 = db.rawQuery("SELECT * FROM Kategorija WHERE _id=?",new String[]{String.valueOf(c.getInt(c.getColumnIndex(ID_KATEGORIJE)))});
                    c2.moveToFirst();
                    String nazivKateg=c2.getString(c2.getColumnIndex(NAZIV));
                    c2.close();

                    k.setKategorija(nazivKateg);

                    rezultat.add(k);


                }
                c.close();

            }while(c3.moveToNext());
            c3.close();

        }
        return rezultat;
    }

    void registrujProcitana(String id){
        ContentValues cv = new ContentValues();
        String query2 = "UPDATE Knjiga SET pregledana=? WHERE idWebServis=?";
        cv.put(PREGLEDANA,1);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query2,new String[]{"1",id});
    }

    //provjerava postoji li knjiga, ako nema dodaje je i onda za sve autore koje nema dodaju u tabelu autori, a generalno sve ih dodaje u autorstvo za ovu knjiug :))))

    long dodajKnjigu(Knjiga knjiga){

        SQLiteDatabase baza = this.getReadableDatabase();
        String query = "SELECT * FROM Knjiga WHERE idWebServis = ?";
        Cursor c = baza.rawQuery(query,new String[]{knjiga.getId()});


        if(c.getCount()>0) return -1; //vec postoji kategorija
        c.close();




        ContentValues cv = new ContentValues();
        cv.put(NAZIV,knjiga.getNaziv());
        cv.put(OPIS,knjiga.getOpis());
        cv.put(DATUM,knjiga.getDatumObjavljivanja());
        cv.put(STRANICE,knjiga.getBrojStrinica());
        cv.put(ID_WEBSERVIS,knjiga.getId());
        cv.put(SLIKA,knjiga.getSlika().toString());


        //TRAZIMO ID KATEGORIJE
        query = "SELECT _id FROM Kategorija WHERE naziv=?";
        c = baza.rawQuery(query,new String[]{knjiga.getKategorija()});
        long idKateg;

        //mora imati ova kategorija
        c.moveToFirst();
        idKateg=c.getInt(c.getColumnIndex(KEY_ID));

        cv.put(ID_KATEGORIJE,idKateg);


        cv.put(PREGLEDANA,0);

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            long noviId =  db.insert(TABELA_KNJIGA, null, cv);

            for(Autor a:knjiga.getAutori()) {
                String upit = "SELECT _id FROM Autor WHERE ime=?";
                Cursor cursor = baza.rawQuery(query,new String[]{a.getImeiPrezime()});
                long autorId=-1;

                //Provjera da li postoji vec autor, ako ne postoji dodamo ga u bazu i uzmemoo njegov id
                if(cursor.getCount()==0){
                    ContentValues cv2 = new ContentValues();
                    cv2.put(IME, a.getImeiPrezime());
                    autorId = db.insert(TABELA_AUTOR, null, cv2);

                }

                //Ako postoji autor, samo mu uzmemo id preko vec postojeceg kursora
                else{
                    cursor.moveToFirst();
                    autorId = cursor.getInt(cursor.getColumnIndex(KEY_ID));

                }

                cursor.close();

                //Dodamo u autorstvo za svakog autora
                ContentValues cv2 = new ContentValues();
                cv2.put(ID_KNJIGE,noviId);
                cv2.put(ID_AUTORA,autorId);
                db.insert(TABELA_AUTORSTVO,null,cv2);

            }

            return noviId;


        }
        catch(Exception e){
            return -1;
        }
    }




    ArrayList<String> DajSveKategorije(){
        ArrayList<String> kategorije = new ArrayList<String>();
        String selectQuery = "SELECT  * FROM " + TABELA_KATEGORIJA;


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                kategorije.add(c.getString(c.getColumnIndex(NAZIV)));
            } while (c.moveToNext());
        }
        c.close();

        return kategorije;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_KATEGORIJA);
        sqLiteDatabase.execSQL(CREATE_TABLE_KNJIGA);
        sqLiteDatabase.execSQL(CREATE_TABLE_AUTOR);
        sqLiteDatabase.execSQL(CREATE_TABLE_AUTORSTVO);
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_KNJIGA);
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_AUTORSTVO);
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_AUTOR);
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_KATEGORIJA);

        db.execSQL(CREATE_TABLE_KNJIGA);
        db.execSQL(CREATE_TABLE_AUTOR);
        db.execSQL(CREATE_TABLE_AUTORSTVO);
        db.execSQL(CREATE_TABLE_KATEGORIJA);
    }
}
