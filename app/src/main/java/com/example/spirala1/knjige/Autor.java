package com.example.spirala1.knjige;

import java.util.ArrayList;

class Autor {

    private String imeiPrezime;
    private ArrayList<String> knjige = new ArrayList<>();

    public Autor(String imeiPrezime, String id) {
        this.imeiPrezime = imeiPrezime;
        this.ime=imeiPrezime;
        this.knjige.add(id);
    }

    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }

    public void dodajKnjigu(String id) {
        if (!knjige.contains(id)) {
            knjige.add(id);
        }
    }

    private String ime;
    private int brojKnjiga;

    public Autor(String ime, int brojKnjiga) {
        this.ime = ime;
        this.imeiPrezime=ime;
        this.brojKnjiga = brojKnjiga;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public int getBrojKnjiga() {
        return brojKnjiga;
    }

    public void setBrojKnjiga(int brojKnjiga) {
        this.brojKnjiga = brojKnjiga;
    }

    public void povecajBrojKnjiga(){
        brojKnjiga++;
    }

    @Override
    public boolean equals(Object o){
        if(((Autor)o).getIme().equals(this.getIme()))
            return true;
        return false;
    }
}
