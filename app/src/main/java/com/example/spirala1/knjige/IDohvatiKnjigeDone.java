package com.example.spirala1.knjige;

import java.util.ArrayList;

public interface IDohvatiKnjigeDone {
    void onDohvatiDone(ArrayList<Knjiga> knjige);
}
