package com.example.spirala1.knjige;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class FragmentOnline extends Fragment {

    EditText query;
    Button pretrazi;
    Button dodajKnjigu;
    Button dPovratak;
    Spinner sKategorije;
    Spinner sRezultat;

    KnjigeReciever resultReciever;
    ArrayAdapter kategorijeAdapter, knjigenaziviadapter;
    ArrayList<String> knjigeNazivLista;
    ArrayList<Knjiga> sveKnjige;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_online, container, false);
        Bundle bundle = getArguments();
        ArrayList<String> kategorije = bundle.getStringArrayList("listaKategorija");
        knjigeNazivLista = new ArrayList<>();


        sveKnjige=new ArrayList<Knjiga>();
        kategorijeAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, kategorije);
        knjigenaziviadapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, knjigeNazivLista);

        query = view.findViewById(R.id.tekstUpit);
        pretrazi = view.findViewById(R.id.dRun);
        dodajKnjigu = view.findViewById(R.id.dAdd);
        dPovratak = view.findViewById(R.id.dPovratak);

        sKategorije = view.findViewById(R.id.sKategorije);
        sRezultat = view.findViewById(R.id.sRezultat);

        sKategorije.setAdapter(kategorijeAdapter);
        sRezultat.setAdapter(knjigenaziviadapter);

        resultReciever = new KnjigeReciever(new Handler(getContext().getMainLooper()));
        resultReciever.setCallBack(new KnjigeReciever.KnjigeReceiverCallBack<Object>() {
            @Override
            public void finish(Object data) {
                ArrayList<Knjiga> lista = (ArrayList<Knjiga>) data;
                ArrayList<String> nazivi = new ArrayList<>();
                for (Knjiga k : lista) {
                    if (!(k.toString().isEmpty()))
                        nazivi.add(k.getNaziv());
                }
                knjigenaziviadapter=new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, nazivi);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sRezultat.setAdapter(knjigenaziviadapter);
                    }
                });

            }



            @Override
            public void error() {

            }

            @Override
            public void start() {

            }
        });

        pretrazi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String queryText = query.getText().toString();
                final ArrayList<String> novaLista = new ArrayList<String>();
                if (queryText.startsWith("korisnik:")) {
                    Intent intent = new Intent(Intent.ACTION_SYNC,null,getActivity(),KnjigePoznanika.class);
                    intent.putExtra("idKorisnika", queryText.replace("korisnik:", ""));
                    intent.putExtra("receiver", resultReciever);
                    getContext().startService(intent);

                } else if (queryText.startsWith("autor:")) {
                    new DohvatiNajnovije(new IDohvatiNajnovijeDone() {
                        @Override
                        public void onNajnovijeDone(ArrayList<Knjiga> knjigaArrayList) {
                            sveKnjige=knjigaArrayList;
                            knjigeNazivLista.clear();
                            for (Knjiga k : knjigaArrayList) {
                                if (!(k.toString().isEmpty()))
                                    novaLista.add(k.toString());
                            }
                            knjigenaziviadapter=new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, novaLista);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    sRezultat.setAdapter(knjigenaziviadapter);
                                }
                            });

                        }
                    }).execute(queryText.replace("autor:", ""));
                } else {
                    String[] listaNaziva = queryText.split(";");
                    new DohvatiKnjige(new IDohvatiKnjigeDone() {
                        @Override
                        public void onDohvatiDone(ArrayList<Knjiga> knjige) {
                            knjigeNazivLista.clear();
                            sveKnjige=knjige;
                            for (Knjiga k : knjige) {
                                if (!(k.toString().isEmpty()))
                                    novaLista.add(k.toString());
                            }
                            knjigenaziviadapter=new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, novaLista);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    sRezultat.setAdapter(knjigenaziviadapter);
                                }
                            });
                        }
                    }).execute(listaNaziva);

                }

            }
        });

        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Knjiga k = sveKnjige.get(sRezultat.getSelectedItemPosition());

                k.setKategorija(sKategorije.getSelectedItem().toString());
                k.setObojena(false);
                long rez = 1;//MainActivity.bk.dodajKnjigu(rezKnjige.get(rezultat.getSelectedItemPosition()));
                if(rez!=-1){
                    KategorijeAkt.baza.dodajKnjigu(sveKnjige.get(sRezultat.getSelectedItemPosition()));
                    KategorijeAkt.refreshujListe();


                    Toast.makeText(getActivity(),"Dodana je knjiga",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getActivity(),"Knjiga nije dodana",Toast.LENGTH_SHORT).show();
                }

            }
        });

        dPovratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }
}
