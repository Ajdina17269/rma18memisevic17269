package com.example.spirala1.knjige;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

public class ListeFragment extends Fragment {

    EditText tekstPretraga;
    Button pretrazi;
    Button dodajKategoriju;
    Button dodajKnjigu;
    Button kategorije;
    Button autori;
    ListView listView;

    ArrayAdapter<String> adapter;

    ListeFragmentInterface listeFragmentInterface;

    public ListeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_liste, container, false);

        tekstPretraga = view.findViewById(R.id.tekstPretraga);
        pretrazi = view.findViewById(R.id.dPretraga);
        dodajKategoriju = view.findViewById(R.id.dDodajKategoriju);
        dodajKnjigu = view.findViewById(R.id.dDodajKnjigu);
        kategorije = view.findViewById(R.id.dKategorije);
        autori = view.findViewById(R.id.dAutori);
        listView = view.findViewById(R.id.listaKategorija);

        listeFragmentInterface = (ListeFragmentInterface)getActivity();

        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, Container.getInstance().kategorije);
        listView.setAdapter(adapter);

        pretrazi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.getFilter().filter(tekstPretraga.getText().toString(), new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int i) {
                        if(adapter.isEmpty()){
                            dodajKategoriju.setEnabled(true);
                            Log.d(Integer.toString(adapter.getCount())  , "onFilterComplete: ");
                        }
                    }
                });
            }
        });

        dodajKategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Container.getInstance().kategorije.add(tekstPretraga.getText().toString());
                KategorijeAkt.baza.dodajKategoriju(tekstPretraga.getText().toString());
                adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, Container.getInstance().kategorije);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                tekstPretraga.getText().clear();
                dodajKategoriju.setEnabled(false);

            }
        });

        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listeFragmentInterface.dodajKnjigu();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Autor a = Container.getInstance().autori.get(i);
                if(pretrazi.getVisibility()==View.GONE)
                    listeFragmentInterface.izlistajKnjige(a.getImeiPrezime(),true);
                else
                    listeFragmentInterface.izlistajKnjige(Container.getInstance().kategorije.get(i),false);
            }
        });

        kategorije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pretrazi.setVisibility(View.VISIBLE);
                dodajKategoriju.setVisibility(View.VISIBLE);
                tekstPretraga.setVisibility(View.VISIBLE);
                ArrayAdapter<String> newAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, Container.getInstance().kategorije);
                listView.setAdapter(newAdapter);
                Container.getInstance().test = false;
            }
        });

        autori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pretrazi.setVisibility(View.GONE);
                dodajKategoriju.setVisibility(View.GONE);
                tekstPretraga.setVisibility(View.GONE);
                ArrayAdapter newAdapter = new ArrayAdapter<Autor>(getActivity(), android.R.layout.simple_list_item_2, android.R.id.text1, Container.getInstance().autori){
                    @NonNull
                    @SuppressLint("SetTextI18n")
                    @Override
                    public View getView(int position, View convertView, @NonNull ViewGroup parent){
                        View view = super.getView(position, convertView, parent);
                        Autor autor = Container.getInstance().autori.get(position);
                        TextView text1 = view.findViewById(android.R.id.text1);
                        TextView text2 = view.findViewById(android.R.id.text2);
                        text1.setText(autor.getIme());
                        text2.setText(Integer.toString(autor.getBrojKnjiga()));
                        return view;
                    }
                };
                listView.setAdapter(newAdapter);
                Container.getInstance().test = true;
            }
        });
        return view;
    }
}
