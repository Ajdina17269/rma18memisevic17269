package com.example.spirala1.knjige;

import java.util.ArrayList;

public class Container {

    private static Container instance;

    ArrayList<String> kategorije;
    ArrayList<Knjiga> knjige;
    ArrayList<Autor> autori;
    Boolean test;
    Boolean pop;

    private Container(){
        kategorije = new ArrayList<>();
        knjige = new ArrayList<>();
        autori = new ArrayList<>();
        test = false;
        pop = true;
    }

    public static Container getInstance(){
        if(instance == null){
            instance = new Container();
        }
        return instance;
    }
}
