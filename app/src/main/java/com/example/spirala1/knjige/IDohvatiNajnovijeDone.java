package com.example.spirala1.knjige;

import java.util.ArrayList;

public interface IDohvatiNajnovijeDone {
    void onNajnovijeDone(ArrayList<Knjiga> knjigaArrayList);
}
