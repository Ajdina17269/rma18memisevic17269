package com.example.spirala1.knjige;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class KnjigeFragment extends Fragment {
    ListView listaKnjiga;
    Button nazad;

    ArrayList<Knjiga> filtrirane;
    AdapterKnjiga adapterKnjiga;

    KnjigeFragmentInterface knjigeFragmentInterface;

    public KnjigeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_knjige, container, false);

        knjigeFragmentInterface = (KnjigeFragmentInterface)getActivity();

        listaKnjiga = view.findViewById(R.id.listaKnjiga);
        nazad = view.findViewById(R.id.dPovratak);


        boolean test = Container.getInstance().test;

        filtrirane = getArguments().getParcelableArrayList("Knjige");


        adapterKnjiga = new AdapterKnjiga(getActivity(), R.layout.list_item_knjiga, filtrirane);
        listaKnjiga.setAdapter(adapterKnjiga);
        listaKnjiga.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                filtrirane.get(i).setObojena(true);
                adapterKnjiga.notifyDataSetChanged();
                Container.getInstance().knjige.get(Container.getInstance().knjige.indexOf(filtrirane.get(i))).setObojena(true);
                KategorijeAkt.baza.registrujProcitana(filtrirane.get(i).getId());
            }
        });

        nazad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                knjigeFragmentInterface.onPovratak();
            }
        });
        return view;
    }
}