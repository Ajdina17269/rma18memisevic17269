package com.example.spirala1.knjige;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PreporuciFragment extends Fragment {

    Knjiga knjiga;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_preporuci,container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        Bundle idKnjige = getArguments();

        String id = idKnjige.getString("idKnjige");

        for(Knjiga k: Container.getInstance().knjige){
            if(k.getId().equals(id))
                knjiga=k;
        }

        final Spinner spinci = getView().findViewById(R.id.sKontakti);
        final ArrayList<String> imena = idKnjige.getStringArrayList("imena");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, idKnjige.getStringArrayList("emailovi"));
        spinci.setAdapter(adapter);

        Button posaljiMail = getView().findViewById(R.id.dPosalji);
        posaljiMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Send email","");
                String[] TO ={spinci.getSelectedItem().toString()};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL,TO);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT,"Procitaj knjigu");


                int n = spinci.getSelectedItemPosition();
                StringBuilder porukaMaila = new StringBuilder("Zdravo "+imena.get(n)+",\n Pročitaj knjigu " + knjiga.getNaziv() + " od ");
                boolean prosaoPrvi=false;
                for(Autor a: knjiga.getAutori()) {
                    if(prosaoPrvi)
                        porukaMaila.append(", ");
                    porukaMaila.append(a.getImeiPrezime());
                    prosaoPrvi=true;

                }
                porukaMaila.append("!");
                emailIntent.putExtra(Intent.EXTRA_TEXT,porukaMaila.toString());
                try{
                    startActivity(Intent.createChooser(emailIntent,"Send email..."));
                    getActivity().finish();
                    Log.i("Finished sending email","");
                }
                catch(android.content.ActivityNotFoundException e){
                    Toast.makeText(getActivity(),"There is no email client installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });



        TextView tNaslov = getView().findViewById(R.id.tNazivKnjige);
        tNaslov.setText(knjiga.getNaziv());

        TextView brStranica=getView().findViewById(R.id.tBrojStranica);
        brStranica.setText(Integer.toString(knjiga.getBrojStrinica()));

        TextView autor = getView().findViewById(R.id.tJedanPisac);
        autor.setText(knjiga.getAutori().get(0).getImeiPrezime());

        TextView datumobjave = getView().findViewById(R.id.tDatumObjave);
        datumobjave.setText(knjiga.getDatumObjavljivanja());



    }
}
