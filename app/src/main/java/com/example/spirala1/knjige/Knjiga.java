package com.example.spirala1.knjige;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

class Knjiga implements Parcelable{

    private String id;
    private String naziv;
    private ArrayList<Autor> autori;
    private String opis;
    private String datumObjavljivanja;
    private URL slika;
    private int brojStrinica;

    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStrinica) {
        this.id = id;
        this.naziv = naziv;
        this.autori = autori;
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.slika = slika;
        this.brojStrinica = brojStrinica;
        this.obojena=false;
    }

    protected Knjiga(Parcel in) {
        id = in.readString();
        naziv = in.readString();
        opis = in.readString();
        datumObjavljivanja = in.readString();
        brojStrinica = in.readInt();
        kategorija = in.readString();
        slikaPath = in.readString();
        byte tmpObojena = in.readByte();
        obojena = tmpObojena == 0 ? null : tmpObojena == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(naziv);
        dest.writeString(opis);
        dest.writeString(datumObjavljivanja);
        dest.writeInt(brojStrinica);
        dest.writeString(kategorija);
        dest.writeString(slikaPath);
        dest.writeByte((byte) (obojena == null ? 0 : obojena ? 1 : 2));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Knjiga> CREATOR = new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int size) {
            return new Knjiga[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public URL getSlika() {
        return slika;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public int getBrojStrinica() {
        return brojStrinica;
    }

    public void setBrojStrinica(int brojStrinica) {
        this.brojStrinica = brojStrinica;
    }

    private String kategorija;
    private String slikaPath;
    private Boolean obojena;

    public Knjiga(String nazivKnjige, String imeAutora, String kategorija, String slika) {
        this.naziv = nazivKnjige;
        this.autori = new ArrayList<>();
        autori.add(new Autor(imeAutora,1));
        this.kategorija = kategorija;
        this.slikaPath = slika;
        obojena = false;


        this.opis = "";
        this.datumObjavljivanja = "";
        this.id=nazivKnjige+imeAutora;
        this.brojStrinica = 0;
        try {
            this.slika = new URL("https://media.gettyimages.com/photos/stack-of-books-picture-id157482029");
        }
        catch(MalformedURLException e){

        }


    }



    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public String getSlikaPath() {
        return slikaPath;
    }

    public void setSlikaPath(String slika) {
        this.slikaPath = slika;
    }

    public Boolean getObojena() {
        return obojena;
    }

    public void setObojena(Boolean obojena) {
        this.obojena = obojena;
    }

    @Override
    public boolean equals(Object o){
        if(((Knjiga) o).getNaziv().equals(naziv) && ((Knjiga) o).getAutori().get(0).getImeiPrezime().equals(autori.get(0).getImeiPrezime()) && ((Knjiga) o).getKategorija().equals(kategorija))
            return true;
        return false;
    }

    @Override
    public String toString() {
        return naziv;
    }
}
